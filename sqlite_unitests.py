__author__ = 'Miguel R.'

import sqlite_test
import unittest

class Test(unittest.TestCase):
    def test_generate_random_mac(self):
        actual = sqlite_test.generate_random_mac()
        print(actual)
        regexp = "[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}"
        self.assertRegex(actual,regexp)


    def test_generate_random_mac2(self):
        actual = sqlite_test.generate_random_mac()
        print(actual)
        regexp = "(?:[0-9A-F]{2}:){5}[0-9A-F]{2}"
        self.assertRegex(actual,regexp)



if __name__ == "__main__":
    unittest.main(exit=False)