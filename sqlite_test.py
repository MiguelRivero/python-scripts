__author__ = 'Miguel R.'

import sqlite3
import random
import re

data_base = "dhcpDB.db"

def generate_random_mac():
    cadena = ""
    for i in range(6):
        par = random.sample(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"], 2)
        cadena += par[0] + par[1]
        if i != 5:
            cadena += ":"
    return cadena

def new_macs(db):
    con = sqlite3.connect(db)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()
    cursor2 = con.cursor()

    cursor.execute("select * from dhcp")
    numero = 0
    for r in cursor:
        new_mac = generate_random_mac()
        sql = "UPDATE dhcp SET mac ='{0}' WHERE id_dhcp='{1}'".format(new_mac,r['id_dhcp'])
        print(sql)
        cursor2.execute(sql)
        numero += 1
        print("Cambiado:"+str(numero))

    con.commit()
    cursor.close()
    cursor2.close()

def fill_blanks(db):
    con = sqlite3.connect(db)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()
    cursor2 = con.cursor()

    cursor.execute("select * from dhcp")
    numero = 0
    for r in cursor:
        if r['mac'] == None :
            new_mac = generate_random_mac()
            new_id_modelo = random.sample(range(1,13),1)[0]
            new_id_biblioteca = random.sample(range(1,21),1)[0]
            ip_list = r['ip'].split(".")
            new_hostname = "T"+ format(new_id_modelo, '02d') + "-" + ip_list[2] + "_" + ip_list[3]
            print(new_hostname)
            id = r['id_dhcp']
            sql = "UPDATE dhcp SET mac='{0}', id_modelo={1}, id_biblioteca={2}, hostname='{3}', dated=(DATETIME('NOW','localtime')) WHERE id_dhcp={4}".format(new_mac, new_id_modelo, new_id_biblioteca,new_hostname,id)
            print(sql)
            cursor2.execute(sql)
            numero += 1
            print("Cambiando:"+str(numero))

    con.commit()
    cursor.close()
    cursor2.close()


if __name__ == "__main__":
    # new_macs(data_base)

    pattern = "(?:[0-9A-F]{2}:){5}[0-9A-F]{2}"
    mac =  "sf dsfsd fd " + generate_random_mac() + " estapalabra sf dsfsd fd fs fsf df"
    print(mac)
    lista = re.search(pattern,mac)
    print("lista : " + str(lista.group()))
    print(type(lista))

    pattern2 = re.compile(r"[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}")
    variable = pattern2.findall(mac)
    print("Final: " + str(variable))

    pattern3 = r".*(?P<ex>[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}) (?P<palabra>\w+) (?P<palabra2>\w+)"
    m = re.match(pattern3,mac)
    a = ({k:v for k,v in zip(["ex","palabra", "otra"],list(m.group("ex", "palabra", "palabra2")))})
    print(a)