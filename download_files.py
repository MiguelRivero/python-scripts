__author__ = "Miguel R."

import re
import time
from functools import partial
from urllib import request
from bs4 import BeautifulSoup


def get_formatted_href(url, regex):
    """(url=str, regexp=str) -> list(str)
    It get all the links in a URL that match a regular expression
    """
    with request.urlopen(url) as req:
        html = req.read()
    soup = BeautifulSoup(html)
    anchors = soup.find_all("a", href=True)
    hrefs = [request.urljoin(url, anchor.get('href')) for anchor in anchors if re.search(regex, anchor.get("href"))]
    return hrefs


def download_file(url):
    """(url=str) -> None

    Download the file given in the url
    """
    file_name = url.split('/')[-1]

    with request.urlopen(url) as file_request, open(file_name, 'wb') as f:
        # get the file_size
        meta = file_request.info()
        file_size = int(meta["Content-Length"])
        print("Downloading: {0}, Size: {1}".format(file_name, size_of_file(file_size)))

        file_size_dl = 0
        BLOCK_SIZE = 8192
        starting_point = time.time()
        for buffer in iter(partial(file_request.read, BLOCK_SIZE), b''):
            file_size_dl += len(buffer)
            f.write(buffer)
            status = "{0:>10}  [{1:>7.2%}]".format(size_of_file(file_size_dl), file_size_dl / file_size)
            print(status, end="\r")
        print(status)
        print(file_name + " downloaded in " + elapsed_time(starting_point, time.time()))


def size_of_file(number):
    """(number=int) -> str
    It calculates the human readable size of a file

    >>> size_of_file(2199023255552)
    '2.00 TB'
    >>> size_of_file(64010352)
    '61.05 MB'
    >>> size_of_file(9518119)
    '9.08 MB'
    >>> size_of_file(1010)
    '1010.00 bytes'
    >>> size_of_file(1025)
    '1.00 KB'
    """
    for unit in ['bytes', 'KB', 'MB', 'GB']:
        if number < 1024.0:
            return "{0:3.2f} {1}".format(number, unit)
        number /= 1024.0
    return "{0:3.2f} {1}".format(number, 'TB')


def elapsed_time(time_start, time_end):
    """(time_start=int, time_end=int) -> str
    Calculate time between 2 times

    >>> elapsed_time(0,1)
    '1 second.'
    >>> elapsed_time(0,35)
    '35 seconds.'
    >>> elapsed_time(0,61)
    '1 minute and 1 second.'
    >>> elapsed_time(0,64)
    '1 minute and 4 seconds.'
    >>> elapsed_time(0,121)
    '2 minutes and 1 second.'
    >>> elapsed_time(0,120)
    '2 minutes and 0 seconds.'
    """
    elapsed_time_int = int(time_end - time_start)
    elapsed_time_minutes = elapsed_time_int // 60
    elapsed_time_seconds = elapsed_time_int % 60
    time = ""
    if elapsed_time_minutes > 1:
        time += str(elapsed_time_minutes) + " minutes and "
    elif elapsed_time_minutes == 1:
        time += str(elapsed_time_minutes) + " minute and "
    if elapsed_time_seconds != 1:
        time += str(elapsed_time_seconds) + " seconds."
    else:
        time += str(elapsed_time_seconds) + " second."
    return time


if __name__ == "__main__":
    import doctest
    doctest.testmod()
    url = "https://moodle.dr-chuck.com/files/lectures/"
    regex = '[\.mp4|\.mov]$'
    array_links = get_formatted_href(url, regex)
    for video in array_links:
        download_file(video)
        print("-------------------------")
    print("Downloading videos completed")